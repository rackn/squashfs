package squashfs

import "io/fs"

type inoType uint16

const (
	DirType inoType = iota + 1
	FileType
	SymlinkType
	BlockDevType
	CharDevType
	FifoType
	SocketType
	XDirType
	XFileType
	XSymlinkType
	XBlockDevType
	XCharDevType
	XFifoType
	XSocketType
)

func (t inoType) IsDir() bool {
	return t == DirType || t == XDirType
}

func (t inoType) IsSymlink() bool {
	return t == SymlinkType || t == XSymlinkType
}

func (t inoType) IsRegular() bool {
	return t == FileType || t == XFileType
}

// Mode returns a fs.FileMode for this type that contains no permissions, only the file's type
func (t inoType) Mode() fs.FileMode {
	switch t {
	case DirType, XDirType:
		return fs.ModeDir
	case FileType, XFileType:
		return 0
	case SymlinkType, XSymlinkType:
		return fs.ModeSymlink
	case BlockDevType, XBlockDevType:
		return fs.ModeDevice // block device
	case CharDevType, XCharDevType:
		return fs.ModeDevice | fs.ModeCharDevice // char device
	case FifoType, XFifoType:
		return fs.ModeNamedPipe
	case SocketType, XSocketType:
		return fs.ModeSocket
	default:
		return fs.ModeIrregular
	}
}
