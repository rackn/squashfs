package squashfs

import (
	"encoding/binary"
	"fmt"
	"io"
	"io/fs"
	"path"
	"strings"
	"sync"
)

const superblockSize = 96

// FS implements squashfs support.
type FS struct {
	src   io.ReaderAt
	order binary.ByteOrder

	rootIno  *inode
	rootInoN uint64
	inoIdx   map[uint32]inodeRef // inode refs cache (see export table)
	inoIdxL  sync.RWMutex
	inoOfft  uint64
	idTable  []uint32

	magic             uint32 // magic identifier
	inodeCnt          uint32 // number of inodes in filesystem
	modTime           int32  // creation unix time as int32 (will stop working in 2038)
	blockSize         uint32 // size of a single data block, must match 1<<blockLog
	fragCount         uint32
	comp              Compression // Compression used, usually GZip
	decompress        Decompressor
	blockLog          uint16
	flags             Flags // squashfs flags
	idCount           uint16
	vMajor            uint16
	vMinor            uint16
	rootInode         inodeRef // inode number/reference of root
	bytesUsed         uint64
	idTableStart      uint64
	xattrIdTableStart uint64
	inodeTableStart   uint64
	dirTableStart     uint64
	fragTableStart    uint64
	exportTableStart  uint64
}

var _ fs.FS = (*FS)(nil)
var _ fs.ReadDirFS = (*FS)(nil)
var _ fs.StatFS = (*FS)(nil)

// Open returns a new instance of FS for a given io.ReaderAt that can
// be used to access files inside squashfs.
func Open(fs io.ReaderAt) (*FS, error) {
	sb := &FS{src: fs,
		inoIdx: make(map[uint32]inodeRef),
	}
	head := make([]byte, superblockSize)

	_, err := fs.ReadAt(head, 0)
	if err != nil {
		return nil, err
	}
	err = sb.UnmarshalBinary(head)
	if err != nil {
		return nil, err
	}

	if sb.vMajor != 4 || sb.vMinor != 0 {
		return nil, ErrInvalidVersion
	}

	// get root inode
	sb.rootIno, err = sb.getInodeRef(sb.rootInode)
	if err != nil {
		return nil, err
	}

	sb.rootInoN = uint64(sb.rootIno.ino)

	err = sb.readIdTable()

	return sb, err
}

func (sq *FS) readIdTable() error {
	// read id table
	idtable, err := sq.newIndirectTableReader(int64(sq.idTableStart), 0)
	if err != nil {
		return err
	}
	var id uint32
	sq.idTable = make([]uint32, sq.idCount)
	for i := range sq.idTable {
		err := binary.Read(idtable, sq.order, &id)
		if err != nil {
			return err
		}
		sq.idTable[i] = id
	}
	//log.Printf("sqashfs: id table = %+v", sq.idTable)
	return nil
}

// UnmarshalBinary reads a binary header values into FS
func (sq *FS) UnmarshalBinary(data []byte) error {
	if len(data) != superblockSize {
		return ErrInvalidSuper
	}

	switch string(data[:4]) {
	case "hsqs":
		sq.order = binary.LittleEndian
	case "sqsh":
		sq.order = binary.BigEndian
	default:
		return ErrInvalidFile
	}

	sq.magic = sq.order.Uint32(data[0:4])
	sq.inodeCnt = sq.order.Uint32(data[4:8])
	sq.modTime = int32(sq.order.Uint32(data[8:12]))
	sq.blockSize = sq.order.Uint32(data[12:16])
	sq.fragCount = sq.order.Uint32(data[16:20])
	sq.comp = Compression(sq.order.Uint16(data[20:22]))
	sq.blockLog = sq.order.Uint16(data[22:24])
	sq.flags = Flags(sq.order.Uint16(data[24:26]))
	sq.idCount = sq.order.Uint16(data[26:28])
	sq.vMajor = sq.order.Uint16(data[28:30])
	sq.vMinor = sq.order.Uint16(data[30:32])
	sq.rootInode = inodeRef(sq.order.Uint64(data[32:40]))
	sq.bytesUsed = sq.order.Uint64(data[40:48])
	sq.idTableStart = sq.order.Uint64(data[48:56])
	sq.xattrIdTableStart = sq.order.Uint64(data[56:64])
	sq.inodeTableStart = sq.order.Uint64(data[64:72])
	sq.dirTableStart = sq.order.Uint64(data[72:80])
	sq.fragTableStart = sq.order.Uint64(data[80:88])
	sq.exportTableStart = sq.order.Uint64(data[88:96])
	decompressMux.Lock()
	sq.decompress = decompressHandlers[sq.comp]
	decompressMux.Unlock()
	if sq.decompress == nil {
		return fmt.Errorf("unsupported compression format %s", sq.comp)
	}

	if sq.magic != 0x73717368 {
		// shouldn't happen
		return ErrInvalidFile
	}

	if uint32(1)<<sq.blockLog != sq.blockSize {
		return ErrInvalidSuper
	}
	return nil
}

// findInode returns the inode for a given path. If followSymlink is false and
// a symlink is found in the path, it will be followed anyway. If however the
// target file is a symlink, then its inode will be returned.
func (sq *FS) findInode(name string, followSymlinks bool) (*inode, error) {
	if !fs.ValidPath(name) {
		return nil, &fs.PathError{Op: "stat", Path: name, Err: fs.ErrInvalid}
	}
	cur := sq.rootIno
	symlinkRedirects := 40 // maximum number of redirects before giving up
	pos := 0
	for {
		toExamine := name[pos:]
		if len(toExamine) == 0 || toExamine == "." {
			return cur, nil
		}
		nextPos := strings.IndexByte(toExamine, '/')
		if nextPos == -1 {
			// no / - perform final lookup
			if !followSymlinks {
				return cur.lookupRelativeInode(toExamine)
			}
			res, err := cur.lookupRelativeInode(toExamine)
			if err != nil {
				return nil, err
			}
			if !res.typ.IsSymlink() {
				return res, nil
			}

			// need to perform symlink lookup, we are not done here
			if symlinkRedirects == 0 {
				return nil, ErrTooManySymlinks
			}
			symlinkRedirects -= 1
			sym, err := res.readLink()
			if err != nil {
				return nil, err
			}
			// ensure symlink isn't empty and isn't absolute either
			if len(sym) == 0 || sym[0] == '/' {
				return nil, fs.ErrInvalid
			}
			// continue lookup from that point
			name = path.Join(name[:pos], sym)
			pos = 0
			if !fs.ValidPath(name) {
				return nil, &fs.PathError{Op: "stat", Path: name, Err: fs.ErrInvalid}
			}
			continue
		}
		if !cur.isDir() {
			return nil, ErrNotDirectory
		}
		t, err := cur.lookupRelativeInode(toExamine[:nextPos])
		if err != nil {
			return nil, err
		}

		if t.typ.IsSymlink() {
			if symlinkRedirects == 0 {
				return nil, ErrTooManySymlinks
			}
			symlinkRedirects -= 1

			sym, err := t.readLink()
			if err != nil {
				return nil, err
			}
			// ensure symlink isn't empty and isn't absolute either
			if len(sym) == 0 || sym[0] == '/' {
				return nil, fs.ErrInvalid
			}
			// prepend symlink to name & remove symlink
			// if symlink a=b and name=a/c it becomes b/c
			name = path.Join(name[:pos], sym, toExamine[nextPos+1:])
			pos = 0
			if !fs.ValidPath(name) {
				return nil, &fs.PathError{Op: "stat", Path: name, Err: fs.ErrInvalid}
			}
			continue
		}
		// there still are further lookups, so this must be a directory
		if !t.isDir() {
			return nil, ErrNotDirectory
		}

		// move forward
		cur = t
		pos += nextPos + 1
	}
}

// Open returns a fs.File for a given path.
func (sq *FS) Open(name string) (fs.File, error) {
	ino, err := sq.findInode(name, true)
	if err != nil {
		return nil, &fs.PathError{Op: "open", Path: name, Err: err}
	}

	return ino.open(path.Base(name)), nil
}

// ReadLink allows reading the value of a symbolic link inside the archive.
func (sq *FS) ReadLink(name string) (string, error) {
	ino, err := sq.findInode(name, true)
	if err != nil {
		return "", &fs.PathError{Op: "readlink", Path: name, Err: err}
	}

	res, err := ino.readLink()
	if err != nil {
		return "", &fs.PathError{Op: "readlink", Path: name, Err: err}
	}
	return res, nil
}

// ReadDir implements fs.ReadDirFS and allows listing any directory inside the archive
func (sq *FS) ReadDir(name string) ([]fs.DirEntry, error) {
	ino, err := sq.findInode(name, true)
	if err != nil {
		return nil, &fs.PathError{Op: "readdir", Path: name, Err: err}
	}
	if !ino.typ.IsDir() {
		return nil, fs.ErrInvalid
	}
	// basic dir, we need to iterate (cache data?)
	dr, err := sq.dirReader(ino)
	if err != nil {
		return nil, err
	}
	return dr.ReadDir(0)
}

// Stat will return stats for a given path inside the squashfs archive
func (sq *FS) Stat(name string) (fs.FileInfo, error) {
	ino, err := sq.findInode(name, true)
	if err != nil {
		return nil, err
	}

	return &namedInode{name: path.Base(name), ino: ino}, nil
}

// Lstat will return stats for a given path inside the squashfs archive. If
// the target is a symbolic link, data on the link itself will be returned.
func (sq *FS) Lstat(name string) (fs.FileInfo, error) {
	ino, err := sq.findInode(name, false)
	if err != nil {
		return nil, err
	}

	return &namedInode{name: path.Base(name), ino: ino}, nil
}
