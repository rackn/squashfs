package squashfs

import "fmt"

type inodeRef uint64

func (i inodeRef) idx() uint32 {
	return uint32((uint64(i) >> 16) & 0xffffffff)
}

func (i inodeRef) offs() uint32 {
	return uint32(uint64(i) & 0xffff)
}

func (i inodeRef) String() string {
	return fmt.Sprintf("inodeRef(index=0x%x,offset=0x%x)", i.idx(), i.offs())
}
