package squashfs

// TODO add buf cache to allow multiple accesses to same block without re-reading
type tableReader struct {
	sb          *FS
	buf         []byte
	offset      int64
	shiftedBase int64 // position of table block list (when blocks aren't one after another)
}

func (sq *FS) newInodeReader(ino inodeRef) (*tableReader, error) {
	return sq.newTableReader(int64(sq.inodeTableStart)+int64(ino.idx()), int(ino.offs()))
}

func (sq *FS) newTableReader(base int64, start int) (*tableReader, error) {
	ir := &tableReader{
		sb:     sq,
		offset: base,
	}

	err := ir.readBlock()
	if err != nil {
		return nil, err
	}

	if start != 0 {
		// need to cut offset
		ir.buf = ir.buf[start:]
	}

	return ir, nil
}

func (sq *FS) newIndirectTableReader(base int64, start int) (*tableReader, error) {
	ir := &tableReader{
		sb:          sq,
		shiftedBase: base,
	}

	err := ir.readBlock()
	if err != nil {
		return nil, err
	}

	if start != 0 {
		// need to cut offset
		ir.buf = ir.buf[start:]
	}

	return ir, nil
}

func (i *tableReader) readBlock() error {
	if i.shiftedBase != 0 {
		// shiftedBase mode
		buf := make([]byte, 8)
		_, err := i.sb.src.ReadAt(buf, i.shiftedBase)
		if err != nil {
			return err
		}
		i.offset = int64(i.sb.order.Uint64(buf))
	}
	buf := make([]byte, 2)
	_, err := i.sb.src.ReadAt(buf, i.offset)
	if err != nil {
		return err
	}
	lenN := i.sb.order.Uint16(buf)
	nocompressFlag := false

	if lenN&0x8000 == 0x8000 {
		// not compressed
		nocompressFlag = true
		lenN = lenN & 0x7fff
	}

	buf = make([]byte, int(lenN))

	// read data
	_, err = i.sb.src.ReadAt(buf, i.offset+2)
	if err != nil {
		return err
	}
	i.offset += int64(lenN) + 2
	if !nocompressFlag {
		// decompress
		buf, err = i.sb.decompress(buf, nil)
		if err != nil {
			return err
		}
	}

	i.buf = buf

	return nil
}

func (i *tableReader) Read(p []byte) (int, error) {
	// read from buf, if empty call readBlock()
	if i.buf == nil {
		err := i.readBlock()
		if err != nil {
			return 0, err
		}
	}

	n := copy(p, i.buf)
	if n == len(i.buf) {
		i.buf = nil
	} else {
		i.buf = i.buf[n:]
	}

	return n, nil
}
