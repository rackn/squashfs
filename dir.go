package squashfs

import (
	"io"
	"io/fs"
)

type dirReader struct {
	sb                          *FS
	r                           *io.LimitedReader
	count, startBlock, inodeNum uint32
}

type direntry struct {
	name string
	typ  inoType // squashfs type
	inoR inodeRef
	sb   *FS
}

func (sq *FS) dirReader(i *inode) (*dirReader, error) {
	tbl, err := i.sb.newTableReader(int64(i.sb.dirTableStart)+int64(i.startBlock), int(i.offset))
	if err != nil {
		return nil, err
	}

	dr := &dirReader{
		sb: i.sb,
		r:  &io.LimitedReader{R: tbl, N: int64(i.sz)},
	}

	return dr, nil
}

func (dr *dirReader) next() (string, inodeRef, error) {
	name, _, inoR, err := dr.nextfull()
	return name, inoR, err
}

func (dr *dirReader) nextfull() (string, inoType, inodeRef, error) {
	// read next entry
	if dr.r.N == 3 {
		return "", 0, 0, io.EOF // probably
	}

	if dr.count == 0 {
		err := dr.readHeader()
		if err != nil {
			return "", 0, 0, err
		}
	}
	var buf [8]byte
	if _, err := io.ReadFull(dr.r, buf[:]); err != nil {
		return "", 0, 0, err
	}
	offset := dr.sb.order.Uint16(buf[0:2])
	typ := inoType(dr.sb.order.Uint16(buf[4:6]))
	siz := dr.sb.order.Uint16(buf[6:8])
	name := make([]byte, int(siz)+1)
	if _, err := io.ReadFull(dr.r, name); err != nil {
		return "", 0, 0, err
	}
	dr.count -= 1

	inoRef := inodeRef((uint64(dr.startBlock) << 16) | uint64(offset))
	return string(name), typ, inoRef, nil
}

func (dr *dirReader) readHeader() error {
	var buf [12]byte
	if _, err := io.ReadFull(dr.r, buf[:]); err != nil {
		return err
	}
	dr.count = dr.sb.order.Uint32(buf[0:4])
	dr.startBlock = dr.sb.order.Uint32(buf[4:8])
	dr.inodeNum = dr.sb.order.Uint32(buf[8:12])
	dr.count += 1
	return nil
}

func (dr *dirReader) ReadDir(n int) ([]fs.DirEntry, error) {
	var res []fs.DirEntry

	for {
		ename, typ, inoR, err := dr.nextfull()
		if err != nil {
			if err == io.EOF {
				return res, nil
			}
			return res, err
		}

		res = append(res, &direntry{ename, typ, inoR, dr.sb})
		if n > 0 && len(res) >= n {
			return res, nil
		}
	}
}

func (de *direntry) Name() string {
	return de.name
}

func (de *direntry) IsDir() bool {
	switch de.typ {
	case 1, 8:
		return true
	default:
		return false
	}
}

func (de *direntry) Type() fs.FileMode {
	return de.typ.Mode()
}

func (de *direntry) Info() (fs.FileInfo, error) {
	// found
	found, err := de.sb.getInodeRef(de.inoR)
	if err != nil {
		return nil, err
	}
	return &namedInode{name: de.name, ino: found}, nil
}
